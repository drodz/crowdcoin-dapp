import Web3 from 'web3';

let web3;

if (typeof window !== 'undefined' && typeof window.web3 !== 'undefined') {
    const getProvider = async () => {
        //await window.ethereum.enable();
        await window.web3.currentProvider.enable(); // request authentication
    };
    getProvider();
    web3 = new Web3(window.web3.currentProvider);
} else {
    const provider = new Web3.providers.HttpProvider(
        process.env.INFURA_URL
    );
    web3 = new Web3(provider);
}

export default web3;
