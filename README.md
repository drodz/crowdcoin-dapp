# crowdcoin-dapp

Kickstarter-like Ethereum dapp created during the Udemy course "Ethereum and Solidity: The Complete Developer's Guide" (https://eylearning.udemy.com/course/ethereum-and-solidity-the-complete-developers-guide/learn/)

## How to run
Run:
```
npm install
```

In root directory create .env file with following keys/values:

| Key                          | Value                                |
| ---------------------------- | ------------------------------------ |
| `SECRET_MNEMONIC`            | Mnemonic for your Ethereum wallet    |
| `INFURA_URL`                 | A link to Infura node                |

Compile contract and deploy it to the blockchain:
```
npm run ethereum:compile
npm run ethereum:deploy
```

Use deployed contract's address and set it to a .env variable:

| Key                          | Value                                |
| ---------------------------- | ------------------------------------ |
| `FACTORY_CONTRACT_ADDRESS`   | Address of deployed contract         |

Run the application:
```
npm start
```
